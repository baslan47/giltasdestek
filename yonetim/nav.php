<?php

if($_SESSION)
{?>
	<nav class="navbar navbar-expand-lg menu_one" id="sticky">
            <div class="container">
                <a class="navbar-brand sticky_logo" href="index.php">
                    <img src="img/giltas-logo-w.png"  alt="logo">
                    <img src="img/giltas-logo.png"  alt="logo">
                </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="menu_toggle">
                        <span class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <span class="hamburger-cross">
                            <span></span>
                            <span></span>
                        </span>
                    </span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav menu ml-auto">
                        <li class="nav-item dropdown submenu ">
                            <a href="index.php" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Anasayfa</a>


                        </li>
												<li class="nav-item dropdown submenu mega_menu tab-demo">
                            <a href="?do=uyeonay" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kayıt</a>


                        </li>

						<li class="nav-item dropdown submenu active">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 18px">
                                Hoşgeldiniz <?php echo $_SESSION["name"]; ?>
                            </a>
                            <i class="arrow_carrot-down_alt2 mobile_dropdown_icon" aria-hidden="false" data-toggle="dropdown"></i>
                            <ul class="dropdown-menu dropdown_menu_two">
                                <li class="nav-item">
                                    <a href="?do=tickets" class="nav-link">
                                        <img src="img/sheet.png" alt="">
                                        <div class="text">
                                            <h5>Mesajlar</h5>

                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="?do=cikis" class="nav-link">
                                        <img src="img/gear.png" alt="">
                                        <div class="text">
                                            <h5>Çıkış Yap</h5>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>


                </div>
            </div>
        </nav>
<?php }
else
{?>
	<nav class="navbar navbar-expand-lg menu_one" id="sticky">
            <div class="container">
                <a class="navbar-brand sticky_logo" href="index.php">
                    <img src="img/giltas-logo-w.png"  alt="logo">
                    <img src="img/giltas-logo.png"  alt="logo">
                </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="menu_toggle">
                        <span class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <span class="hamburger-cross">
                            <span></span>
                            <span></span>
                        </span>
                    </span>
                </button>


            </div>
        </nav>
<?php }
?>
