<?php
	$sayfa=intval(@$_GET["sayfa"]);
	if(!$sayfa)
	{
		$sayfa=1;
	}
	$s=$db->prepare("select * from user where onay=0");
	$s->execute();
	$s->fetchALL(PDO::FETCH_ASSOC);
	$toplam=$s->rowCount();
	$limit=10;
	$goster=$sayfa*$limit-$limit;
	$sayfa_sayisi=ceil($toplam/$limit);
	$forlimit=3;
?>
<section class="breadcrumb_area">
            <img class="p_absolute bl_left" src="img/v.svg" alt="">
            <img class="p_absolute bl_right" src="img/home_one/b_leaf.svg" alt="">
            <img class="p_absolute star" src="img/home_one/banner_bg.png" alt="">
            <img class="p_absolute wave_shap_one" src="img/blog-classic/shap_01.png" alt="">
            <img class="p_absolute wave_shap_two" src="img/blog-classic/shap_02.png" alt="">
            <img class="p_absolute one wow fadeInRight" src="img/home_one/b_man_two.png" alt="">
            <img class="p_absolute two wow fadeInUp" data-wow-delay="0.2s" src="img/home_one/flower.png" alt="">
            <div class="container">
                <div class="breadcrumb_content_two text-center">
					<h2>Onaylanmayı Bekleyen Kayıtlar</h2>
                </div>
            </div>
        </section>


        <section class="doc_blog_grid_area sec_pad forum-page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- /.post-header -->
						<?php
						$tickets=$db->prepare("select * from user where onay=0");
						$tickets->execute();
						$x=$tickets->fetchALL(PDO::FETCH_ASSOC);

						foreach($x as $m)
						{
						?>
                        <div class="community-posts-wrapper bb-radius" >
                            <div class="community-post style-two docly richard bug">
                                <div class="post-content">
                                    <div class="author-avatar">
                                        <img src="img/forum/musteri.png" alt="community post">
                                    </div>
                                    <div class="entry-content">
                                        <h3 class="post-title">
                                            <a href="#"><?php echo $m["userName"].' '.$m["userSurname"].' - '.$m["userCompany"]; ?></a>
                                        </h3>



                                        <ul class="meta">


                                            <li><i class="icon_calendar"></i><?php echo $m["userMail"];?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="post-meta-wrapper">
                                    <ul class="post-meta-info">
                                        <!--<li><a href="#"><i class="icon_chat_alt"></i>Okunmadı</a></li> -->
                                        <li><a onClick="return confirm('Kayıt Onaylansın mı ?')" href="?do=onayla&id=<?php echo $m["ID"] ?>"><i class="icon_star" style="color: forestgreen"></i>Onayla</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- /.community-posts-wrapper -->
						<?php } ?>
                        <div class="pagination-wrapper">

                            <ul class="post-pagination">
                            <?php
						      for($i = $sayfa - $forlimit; $i<$sayfa + $forlimit +1; $i++ )
                              {
	                               if($i>0 && $i<=$sayfa_sayisi)
	                               {
		                              if($i == $sayfa)
		                              {
			                             echo '<li><a href="#" class="active">'.$i.'</a></li>';
		                              }
                                        else
		                              {
			                             echo '<li><a href="?do=anasayfa&sayfa='.$i.'">'.$i.'</a></li>';
		                              }
	                               }
                            }
                        if($sayfa != $sayfa_sayisi)
                        {
	                           echo '<li class="next-post"><a href="?do=anasayfa&sayfa='.$sayfa_sayisi.'"><i class="arrow_carrot-right"></i></a></li></ul>';

                        }
						?>
                            <!--
                            <ul class="post-pagination">

                                <li class="prev-post pegi-disable"><a href="#"><i class="arrow_carrot-left"></i></a>
                                </li>
                                <li><a href="#" class="active">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">15</a></li>
                                <li class="next-post"><a href="#"><i class="arrow_carrot-right"></i></a></li>
                            </ul>
                            -->
                        </div>
                        <!-- /.pagination-wrapper -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
            </div>
        </section>
