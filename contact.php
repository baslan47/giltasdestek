<section class="doc_banner_area_one">
            <img class="p_absolute star" src="img/home_one/banner_bg_two.png" alt="">
            <img class="dark" src="img/home_one/wave_one.svg" alt="">
            <img class="dark_two" src="img/home_one/wave_two.svg" alt="">
            <img class="p_absolute star_one" src="img/home_one/star.png" alt="">
            <img class="p_absolute star_two" src="img/home_one/star.png" alt="">
            <img class="p_absolute star_three" src="img/home_one/star.png" alt="">
            <img class="p_absolute one wow fadeInLeft" data-wow-delay="0.1s" src="img/home_one/b_man.png" alt="">
            <img class="p_absolute two wow fadeInRight" data-wow-delay="0.2s" src="img/home_one/b_man_two.png" alt="">
            <img class="p_absolute three wow fadeInUp" data-wow-delay="0.3s" src="img/home_one/flower.png" alt="">
            <img class="p_absolute four wow fadeInRight" data-wow-delay="0.4s" src="img/home_one/girl_img.png" alt="">
            <img class="p_absolute five wow fadeIn" data-wow-delay="0.5s" src="img/home_one/file.png" alt="">
            <img class="p_absolute bl_left" src="img/v.svg" alt="">
            <img class="p_absolute bl_right" src="img/home_one/b_leaf.svg" alt="">
            <div class="container">
                <div class="section_title text-left">
                    <h2 class="h_title wow fadeInUp">Bize Ulaşın?</h2>
                </div>
                <div class="get_info_inner">
                    <div class="row get_info_item align-items-center justify-content-between" style="background-color: white">
                        <div class="col-lg-4 col-sm-5" >
                            <div class="media">
                                <img src="img/email.png" alt="">
                                <div class="media-body">
                                    <h5 class="h5 bold">E-mail</h5>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-7 d-flex align-items-center justify-content-between">
                            <div class="time">
                               <span>destek@giltas.com.tr</span>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="row get_info_item align-items-center justify-content-between" style="background-color: white">
                        <div class="col-lg-4 col-sm-5">
                            <div class="media">
                                <img src="img/community.png" alt="">
                                <div class="media-body">
                                    <h5 class="h5 bold">Telefon</h5>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-7 d-flex align-items-center justify-content-between">
                            <div class="time">
                                <span>0 (232) 446 82 52</span>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
