;(function ($) {
    "use strict";

    /*============= preloader js css =============*/
    var cites = [];
    cites[0] = "Müşterilerini, müşterileri için kaçınılmaz kılar.";
    cites[1] = "Giltaş A.Ş. - 1986 - ";
    cites[2] = "Giltaş A.Ş. Destek Portalı";
    cites[3] = "Hakları tamamen saklıdır. ";
    var cite = cites[Math.floor(Math.random() * cites.length)];
    $('#preloader p').text(cite);
    $('#preloader').addClass('loading');

    $(window).on( 'load', function() {
        setTimeout(function () {
            $('#preloader').fadeOut(500, function () {
                $('#preloader').removeClass('loading');
            });
        }, 500);
    })

})(jQuery)