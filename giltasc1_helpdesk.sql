-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost:3306
-- Üretim Zamanı: 13 Tem 2021, 11:36:03
-- Sunucu sürümü: 5.6.48
-- PHP Sürümü: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `giltasc1_helpdesk`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mesajlar`
--

CREATE TABLE `mesajlar` (
  `ID` int(50) NOT NULL,
  `mesajDesc` varchar(100) COLLATE utf32_bin NOT NULL,
  `mesajGonderen` int(50) NOT NULL,
  `mesajKime` int(100) NOT NULL,
  `mesajTarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mesajOkunma` int(50) NOT NULL DEFAULT '0',
  `IDticket` varchar(50) COLLATE utf32_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

--
-- Tablo döküm verisi `mesajlar`
--

INSERT INTO `mesajlar` (`ID`, `mesajDesc`, `mesajGonderen`, `mesajKime`, `mesajTarih`, `mesajOkunma`, `IDticket`) VALUES
(2, 'test', 1, 1, '2020-10-20 22:04:03', 0, '1'),
(3, 'test test', 1, 1, '2020-10-20 22:24:26', 0, '1'),
(4, 'test test', 1, 1, '2020-10-20 22:29:23', 0, '1'),
(5, 'test test ', 1, 1, '2020-10-20 22:29:32', 0, '1'),
(6, 'test test ', 1, 2, '2020-10-20 22:44:08', 0, '7'),
(7, 'deneme deneme', 2, 1, '2020-10-20 22:45:34', 0, '7'),
(8, 'mesaj test', 1, 2, '2020-11-06 01:23:22', 0, '14'),
(9, 'test mesaj', 2, 1, '2020-11-06 01:23:48', 0, '14'),
(10, 'Soz konusu Stored Procedure\'de hata gozüküyor. Çozumu ıcın ilgileniyorum . - Giltaş Bertan', 1, 3, '2020-11-06 15:37:47', 0, '15'),
(11, 'deneme deneme ', 1, 2, '2020-11-09 06:49:03', 0, '16');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `tickets`
--

CREATE TABLE `tickets` (
  `ID` int(10) NOT NULL,
  `ticDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `oncelik` int(5) NOT NULL,
  `konu` varchar(100) COLLATE utf8_bin NOT NULL,
  `mesaj` mediumtext COLLATE utf8_bin NOT NULL,
  `file1` varchar(100) COLLATE utf8_bin NOT NULL,
  `file2` varchar(100) COLLATE utf8_bin NOT NULL,
  `userID` int(10) NOT NULL,
  `cozum` int(10) NOT NULL,
  `category` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Tablo döküm verisi `tickets`
--

INSERT INTO `tickets` (`ID`, `ticDate`, `oncelik`, `konu`, `mesaj`, `file1`, `file2`, `userID`, `cozum`, `category`) VALUES
(1, '2020-09-01 18:00:00', 1, 'deneme', 'deneme', '', '', 1, 0, 2),
(2, '2020-09-02 16:08:24', 2, 'test', 'test', '', '', 1, 0, 5),
(3, '2020-09-02 17:48:05', 3, 'deneme test', 'deneme test ornek deneme test ornek deneme test ornek deneme test ornek deneme test ornek deneme test ornek deneme test ornek', '', '', 1, 0, 5),
(4, '2020-09-03 04:57:39', 2, 'deneme', 'deneme deneme', '', '', 1, 0, 2),
(5, '2020-09-07 20:43:56', 1, 'testr', 'asdasd', '', '', 1, 0, 2),
(6, '2020-09-07 20:46:44', 1, 'sasd', 'asdasd asd', '', '', 1, 0, 2),
(7, '2020-09-07 20:51:15', 2, 'dfsda dsfa ', 'sadfas  fas', '', '', 2, 0, 5),
(8, '2020-10-06 14:36:29', 3, 'Gm\'de Fotograf eklenmiyor', 'merhaba \r\ngmde  fotograf eklenmiyor.\r\nkaydetmeye basıyoruz ancak dizine yazmıyor.', '', '', 3, 0, 3),
(9, '2020-10-09 08:02:09', 2, 'Sipariş ekranında odeme planları ürünlerin yanında yazsın', 'Sipariş ekranın.da odeme planları ürünlerin yanında yazsın \r\n\r\n', '', '', 3, 0, 3),
(10, '2020-10-09 08:03:56', 3, 'peşin satış siparişte hata', 'peşin satış siparişte ilk girişte ürüne ait tüm odeme planları geliyor . sadece peşini ilgilendirenler gelmeli', '', '', 3, 0, 3),
(11, '2020-10-09 08:05:11', 2, 'ürün ana ekranında hata', 'ürünleri ürün sayfasında okuttugumuzda urunun alfabeye gore ilk rengini geitiiyor.\r\nbarkod rengi bedeni gelmiyor.\r\nbarkod rengi bedeni gelmeli', '', '', 3, 0, 3),
(12, '2020-10-09 08:07:02', 3, 'MT satışında  miktar küsurlu olunca hesap yapmıyor', 'ürünlerde miktar mt olunca 2,8 gibi hesaplama yapmıyor.', '', '', 3, 0, 3),
(13, '2020-10-13 12:01:33', 2, 'Sipariş Raporuna İstekler', 'merhaba\r\nmağazada gün içinde girilen siparişlerin gelmesi isteniyor.(kullanıcının bağlı olduğu mağaza bilgisi çekilecek)\r\nsp:G3_WholesaleRetailOrders\r\nburada tüm siparişler geliyor.', '', '', 3, 0, 3),
(14, '2020-11-06 01:20:41', 1, 'mesaj test', 'mesaj test', '', '', 2, 0, 3),
(15, '2020-11-06 14:16:58', 2, 'Gm\'de kefil eklenmiyor', 'merhaba \r\nprogram üzerinde ekleme yapıyor gibi davranıyor\r\nancak v3e aktarmıyor', '', '', 3, 1, 3),
(16, '2020-11-09 06:47:25', 1, 'test test', 'test test test', '', '', 2, 1, 3),
(17, '2020-11-12 06:59:25', 3, 'test', 'test', '', '', 4, 0, 3),
(18, '2020-11-12 07:00:46', 1, 'sa', 'dsa', '', '', 4, 0, 1),
(19, '2020-11-18 14:21:39', 3, 'aynı kimlik numarası ile müşteri ve kefil kayıt etme', 'Nüket hanım merhaba\r\nBursa mağazamızda tablet işleminde alıcı ve kefil aynı kimlik numarası ile kaydedilmiş ve sistem hata vermemiştir. Gerekli düzeltmeyi yapıyoruz.', 'dosya/191411.png', '', 7, 0, 3),
(20, '2020-11-25 14:53:12', 2, 'gm askıdan alma işlemi', 'Merhabalar,\r\nNokta mağazamızda kasiyer arkadaş askıdan al ekranında diğer mağazaların da satışlarını görebilmektedir. Konuyla ilgili inceleme yaparak problemin giderilmesini rica ederim. ', 'dosya/235868.png', '', 7, 0, 3),
(21, '2020-11-26 14:12:07', 2, '', 'Merhabalar,\r\nNarlıdere (S14) mağazamızdan bir kullanıcı (10612 kullanıcı kodlu) için aşağıdaki şekilde bir şikayet ulaşmıştır. Diğer kullanıcılarda sorun gözlemlenmezken sadece bir kullanıcının yaptığı satışlar askıya 2-3 dakika gibi gecikmeli olarak düştüğü iletilmiştir.\r\nKonuyu inceleyerek bilgi verir misiniz ?', 'dosya/238997', '', 7, 0, 3),
(22, '2020-12-03 12:37:54', 3, 'Gm Hata Ve İstekler', 'merhaba\r\nhatalar & İstekler ektedir', 'dosya/77221docx', '', 3, 0, 3),
(23, '2020-12-03 12:40:06', 3, 'hata İstekler', 'Müşteri\r\n•	Arama ekranı toptana uygun olmalı (vergi no- firma unvanı vs olmalı)\r\n•	Toptan müşteri açarken Adı- Soyadı yerine Firma Unvanı olmalı\r\n•	Tc zorunlu olmamalı  \r\n•	Kişisel Bilgilere gerek yok\r\nSipariş\r\n•	Sipariş ekranında beden matrixi isteniyor\r\n•	Bedenlerin yanında kullanılabilir envanter görünmesi isteniyor \r\n.        Satış sorumlusuna atanan müşteriler arama ekranına gelecek\r\nRaporlar\r\n•	Müşteri ekstresi görünmüyor\r\n•	Sipariş raporunda satış sorumlusunun aldığı siparişleri görsün istiyorlar\r\n•	Açık siparişleri raporları almak istiyorlar\r\n', 'dosya/990731', '', 3, 0, 3),
(24, '2020-12-10 14:31:32', 3, 'mağaza ofis adı değişimi', 'merhaba,\r\nAşağıda göndermiş olduğum satış tabletten yapılmış ve mağaza ofis adı Salihli iken Merkez Ofis olarak değişmiştir. \r\nDenizli , Aydın, Balıkesir Salihli, Turgutlu gibi diğer mağazalar da kontrol edilmiştir ve sorun aynı şekilde diğer mağazalarda devam etmektedir. \r\nProblemin kontrolünü rica ederim. \r\n\r\nNot:\r\nBazı diğer evraklar. \r\n\r\n1-4-247908\r\n701027624\r\n701037802\r\n701130536\r\n2-4-35245\r\n2-4-338677\r\n701107080\r\n2-4-158085', 'dosya/537061.png', '', 7, 0, 3),
(25, '2020-12-29 12:38:20', 3, 'satıcı kodu çıkmaması problemi ', 'Satıcı kodunun çıkmaması problemi halen devam etmektedir. güncel versiyon ile şirinyer mağazanın yaşadığı problemi ve ekran görüntülerini ek’ te gönderiyorum. Konuyu tekrar incelemenizi rica ederim.', 'dosya/305764.png', '', 7, 0, 3),
(26, '2020-12-30 12:43:20', 3, 'tablet işleminin askı yerine direk müşteri hesabına işlenmesi', 'Siparişin askı yerine direk olarak müşteri hesabına işlenmesi problemi de halen devam ediyormuş. Bornova züccaciye mağazası aşağıdaki şekilde bir problem iletti. Kontrol sağlar mısınız ?\r\n                İlginiz için teşekkür ederim.\r\nmerhabalar,\r\n\r\nbornova züccaciye mağazamız aşağıdaki problemi iletmiştir.\r\nTuğçe Şimşek 10402 satıcı kodu işlemi yapan personel 700295905 şükran kokdaş müşterisinin 730 tl lik işlemi tabletten sisteme atılmasına rağmen askıda gözükmemiştir\r\n', 'dosya/322309', '', 7, 0, 3),
(27, '2021-01-24 14:43:18', 3, 'Gm4 telefonda çalışmıyor ', 'Gm\'nin yeni versiyonu telefonda çalışmıyor ', 'dosya/986417', '', 3, 0, 3),
(28, '2021-06-13 11:42:13', 3, 'APK 2,0,9 Hataları', 'merhaba \r\nhatalar aşağıdaki gibidir;\r\n*Girişte class not found hatası vermektedir.\r\n*Müşteri kaydederken giriş dizesi doğru değil &hesabın varsayılan adresi bulunmadı hatası veriyor müşteri kartı açılamıyor\r\n*peşin satış ekranında taksit tutar bilgisi olmamalı..\r\n*satış tiplerinin içinde perakende satış ve peşin satış hemen teslim aynı satış tipi peşin satış hemen teslim olan kaldırılabilir ,kafa karışıklığı yaratmaması adına\r\n*müşteri ekstresi çalışmıyor\r\n*sipariş raporu sadece toptan siparişleri getiriyor\r\n', 'dosya/577522', '', 3, 0, 3),
(29, '2021-07-07 10:47:17', 3, 'GM ile Sayım Yapma', 'Merhabalar , \r\nFirmaların istekleri ve talepleri doğrultusunda depo sayımı yapabilmek için günümüzde el terminali kullanılmaktadır. \r\nBurada firmalar bu maliyetten kaçmak için ek uygulamalar aramaktadır. Bu yüzden Gm ile online sayım yapabilmek ve kameradan barkod okutularak sayım yapabilirmiyiz. Hem pratik bir uygulama olur hemde yıl sonuna kadar 10 adet firmaya çok rahat bunu satabilirim. ', 'dosya/332604', '', 8, 0, 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user`
--

CREATE TABLE `user` (
  `ID` int(50) NOT NULL,
  `userName` varchar(100) COLLATE utf32_bin NOT NULL,
  `userSurname` varchar(100) COLLATE utf32_bin NOT NULL,
  `userPassword` varchar(100) COLLATE utf32_bin NOT NULL,
  `userMail` varchar(100) COLLATE utf32_bin NOT NULL,
  `userCompany` varchar(100) COLLATE utf32_bin NOT NULL,
  `token` varchar(100) COLLATE utf32_bin NOT NULL,
  `onay` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

--
-- Tablo döküm verisi `user`
--

INSERT INTO `user` (`ID`, `userName`, `userSurname`, `userPassword`, `userMail`, `userCompany`, `token`, `onay`) VALUES
(1, 'betan', 'aslan', 'e10adc3949ba59abbe56e057f20f883e', 'b.aslan1912@hotmail.com', 'Giltaş A.Ş.', 'k9gAKvPUbxhcofhGR3TfFRcK3RhFE7DA', 1),
(2, 'bertan', 'aslan', 'e10adc3949ba59abbe56e057f20f883e', 'baslan@giltas.com.tr', 'giltas', 'nvpYFDiv0hTWUjvd9bloUT7eY5ELySmC', 1),
(3, 'Betül', 'Seçkin', '02cb7d6daaf8d934967f1b9e981ac77c', 'bdoygun@giltas.com.tr', '', '5HmI3ARpVB5lMMBrtJk3NXg6v6fwYh1N', 1),
(4, 'Deneme1', 'Deneme', '02cb7d6daaf8d934967f1b9e981ac77c', 'betuldoygun@giltas.com.tr', 'ozsanal', '3rwNNYIoBFFMA4UfWv3MBIKIghGVJhv1', 1),
(5, 'test', 'test', 'e10adc3949ba59abbe56e057f20f883e', 'test@test.com.tr', 'test ', 'iM6d6Gd7iBD7N2ImPJ3YOhAY92IvudvP', 1),
(6, 'deneme', 'deneme', 'e10adc3949ba59abbe56e057f20f883e', 'deneme@deneme.com.tr', 'deneme', 'YSRfUtKdxUW0E6wm1xUDu5MOwAtR8vN3', 0),
(7, 'lokman', 'koroglu', 'af03927965e5d312b8e061ec0416c469', 'lokman.koroglu@ozsanal.com.tr', 'Ozsanal', '0GRV7X549Mf2lvMOLuCKPMnGgvJoMAIO', 1),
(8, 'ANIL', 'Ç.', 'e10adc3949ba59abbe56e057f20f883e', 'acignakli@giltas.com.tr', '', 'MU5p85CESc0NH8B5tn6KkfpNywVnnMYB', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yonetim`
--

CREATE TABLE `yonetim` (
  `ID` int(50) NOT NULL,
  `userName` varchar(100) COLLATE utf32_bin NOT NULL,
  `userPassword` varchar(100) COLLATE utf32_bin NOT NULL,
  `userMail` varchar(100) COLLATE utf32_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_bin;

--
-- Tablo döküm verisi `yonetim`
--

INSERT INTO `yonetim` (`ID`, `userName`, `userPassword`, `userMail`) VALUES
(1, 'Giltaş', 'e10adc3949ba59abbe56e057f20f883e', 'destek@giltas.com.tr');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `mesajlar`
--
ALTER TABLE `mesajlar`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- Tablo için indeksler `yonetim`
--
ALTER TABLE `yonetim`
  ADD PRIMARY KEY (`ID`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `mesajlar`
--
ALTER TABLE `mesajlar`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Tablo için AUTO_INCREMENT değeri `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Tablo için AUTO_INCREMENT değeri `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Tablo için AUTO_INCREMENT değeri `yonetim`
--
ALTER TABLE `yonetim`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
