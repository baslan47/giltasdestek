<section class="doc_banner_area_one">
            <img class="p_absolute star" src="img/home_one/banner_bg_two.png" alt="">
            <img class="dark" src="img/home_one/wave_one.svg" alt="">
            <img class="dark_two" src="img/home_one/wave_two.svg" alt="">
            <img class="p_absolute star_one" src="img/home_one/star.png" alt="">
            <img class="p_absolute star_two" src="img/home_one/star.png" alt="">
            <img class="p_absolute star_three" src="img/home_one/star.png" alt="">
            <img class="p_absolute one wow fadeInLeft" data-wow-delay="0.1s" src="img/home_one/b_man.png" alt="">
            <img class="p_absolute two wow fadeInRight" data-wow-delay="0.2s" src="img/home_one/b_man_two.png" alt="">
            <img class="p_absolute three wow fadeInUp" data-wow-delay="0.3s" src="img/home_one/flower.png" alt="">
            <img class="p_absolute four wow fadeInRight" data-wow-delay="0.4s" src="img/home_one/girl_img.png" alt="">
            <img class="p_absolute five wow fadeIn" data-wow-delay="0.5s" src="img/home_one/file.png" alt="">
            <img class="p_absolute bl_left" src="img/v.svg" alt="">
            <img class="p_absolute bl_right" src="img/home_one/b_leaf.svg" alt="">
            <div class="container">
                <div class="communities-boxes">
                    <div class="docly-com-box wow fadeInRight" data-wow-delay="0.5s">
                        <div class="icon-container">
                            <img src="img/home_support/rc1.png" alt="communinity-box">
                        </div>
                        <div class="docly-com-box-content">
							<?php if($_SESSION) { ?>
								<h3 class="title"><a href="?do=ticketadd&category=1"> Genel</a></h3>
							<?php }else { ?>
								<h3 class="title"><a href="?do=talepshow&category=1"> Genel</a></h3>
							<?php } ?>
                            
                            
                        </div>
                        <!-- /.docly-com-box-content -->
                    </div>
                    <!-- /.docly-com-box -->

                    <div class="docly-com-box wow fadeInRight" data-wow-delay="0.7s">
                        <div class="icon-container">
                            <img src="img/home_support/rc2.png" alt="communinity-box">
                        </div>
                        <div class="docly-com-box-content">
                            <?php if($_SESSION) { ?>
								<h3 class="title"><a href="?do=ticketadd&category=2"> Sistem Destek (Bilgi İşlem)</a></h3>
							<?php }else { ?>
								<h3 class="title"><a href="?do=talepshow&category=2"> Sistem Destek (Bilgi İşlem)</a></h3>
							<?php } ?>
                            
                        </div>
                        <!-- /.docly-com-box-content -->
                    </div>
                    <!-- /.docly-com-box -->

                    <div class="docly-com-box wow fadeInRight" data-wow-delay="0.9s">
                        <div class="icon-container">
                            <img src="img/home_support/rc3.png" alt="communinity-box">
                        </div>
                        <div class="docly-com-box-content">
                            <?php if($_SESSION) { ?>
								<h3 class="title"><a href="?do=ticketadd&category=3"> Yazılım (GM - Akıllı Kabin)</a></h3>
							<?php }else { ?>
								<h3 class="title"><a href="?do=talepshow&category=3"> Yazılım (GM - Akıllı Kabin)</a></h3>
							<?php } ?>
                           
                        </div>
                        <!-- /.docly-com-box-content -->
                    </div>
                    <!-- /.docly-com-box -->

                    <div class="docly-com-box wow fadeInRight" data-wow-delay="1.1s">
                        <div class="icon-container">
                            <img src="img/home_support/rc4.png" alt="communinity-box">
                        </div>
                        <div class="docly-com-box-content">
                            <?php if($_SESSION) { ?>
								<h3 class="title"><a href="?do=ticketadd&category=4"> Raporlama</a></h3>
							<?php }else { ?>
								<h3 class="title"><a href="?do=talepshow&category=4"> Raporlama</a></h3>
							<?php } ?>
                            
                        </div>
                        <!-- /.docly-com-box-content -->
                    </div>
                    <!-- /.docly-com-box -->

                    <div class="docly-com-box wow fadeInRight" data-wow-delay="1.3s">
                        <div class="icon-container">
                            <img src="img/home_support/rc5.png" alt="communinity-box">
                        </div>
                        <div class="docly-com-box-content">
                            <?php if($_SESSION) { ?>
								<h3 class="title"><a href="?do=ticketadd&category=5"> Satış</a></h3>
							<?php }else { ?>
								<h3 class="title"><a href="?do=talepshow&category=5"> Satış</a></h3>
							<?php } ?>
                            
                        </div>
                        <!-- /.docly-com-box-content -->
                    </div>
                    <!-- /.docly-com-box -->

                </div>
            </div>
        </section>